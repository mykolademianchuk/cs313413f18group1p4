package edu.luc.etl.cs313.android.simplestopwatch.model.state;

import edu.luc.etl.cs313.android.simplestopwatch.R;

class StoppedState implements StopwatchState {

  private final StopwatchSMStateView sm;

  public StoppedState(final StopwatchSMStateView sm) {
    this.sm = sm;
  }

  @Override
  public void onStartStop() {
    sm.actionReset();
    sm.actionInc();
    sm.actionStart();
    sm.toThreeSecondDelayState();
  }

  @Override
  public void onTick() {
    throw new UnsupportedOperationException("onTick");
  }

  @Override
  public void updateView() {
    sm.updateUIRuntime();
  }

  @Override
  public int getId() {
    return R.string.STOPPED;
  }

  @Override
  public int getButtonId() {
    return R.string.STOPPED_BUTTON;
  }
}
