package edu.luc.etl.cs313.android.simplestopwatch.model.state;

import edu.luc.etl.cs313.android.simplestopwatch.R;

public class AlarmSoundingState implements StopwatchState {

  private final StopwatchSMStateView sm;

  public AlarmSoundingState(final StopwatchSMStateView sm) {
    this.sm = sm;
  }

  @Override
  public void updateView() {
    sm.updateUIRuntime();
  }

  @Override
  public int getId() {
    return R.string.ALARM_SOUNDING;
  }

  @Override
  public int getButtonId() {
    return R.string.ALARM_SOUNDING_BUTTON;
  }

  @Override
  public void onStartStop() {
    sm.actionStop();
    sm.actionReset();
    sm.toStoppedState();
  }

  @Override
  public void onTick() {
    playDefaultNotification();

  }

  private void playDefaultNotification() {
    sm.actionPlayBeep();
  }

}
