package edu.luc.etl.cs313.android.simplestopwatch.model.state;

import edu.luc.etl.cs313.android.simplestopwatch.R;

public class ThreeSecondDelayState implements StopwatchState {

  private static int DELAY_TICKS = 3;
  private final StopwatchSMStateView sm;
  private int ticksLeft = DELAY_TICKS;

  public ThreeSecondDelayState(final StopwatchSMStateView sm) {
    this.sm = sm;
  }

  private void resetTicksLeft() {
    this.ticksLeft = DELAY_TICKS;
  }

  @Override
  public void updateView() {
    sm.updateUIRuntime();
  }

  @Override
  public int getId() {
    return R.string.THREE_SECOND_DELAY;
  }

  @Override
  public int getButtonId() {
    return R.string.THREE_SECOND_DELAY_BUTTON;
  }

  @Override
  public void onStartStop() {
    this.resetTicksLeft();
    sm.actionInc();
    if (sm.actionGetRuntime() == 99) {
      sm.actionPlayBeep();
      sm.toRunningState();
    }
  }

  @Override
  public void onTick() {
    ticksLeft--;
    if (ticksLeft == 0) {
      sm.actionPlayBeep();
      sm.toRunningState();
      this.resetTicksLeft();
    }
    if (ticksLeft < 0) {
      throw new IllegalStateException("Ticks may not be negative");
    }
  }
}
